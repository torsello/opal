name := "Dependencies Extraction Library"

version := "0.8.1-SNAPSHOT"

scalacOptions in (Compile, doc) := Opts.doc.title("OPAL - Dependencies Extraction Library") 